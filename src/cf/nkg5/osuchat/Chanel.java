package cf.nkg5.osuchat;


import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class Chanel extends Tab{
	private String name;
	private TextFlow text;
	private TextField message;
	private Button submit;
	
	public Chanel(String name,boolean close){
		super(name);
		super.setClosable(close);
		BorderPane layout=new BorderPane();
		super.setContent(layout);
		this.name=name;
		text=new TextFlow();
		text.setPadding(new Insets(5));
		message=new TextField();
		message.setOnAction(e->submit());
		submit=new Button("Send");
		submit.setOnAction(e->submit());
		ScrollPane scroll=new ScrollPane(text);
		layout.setCenter(scroll);
		BorderPane messagePane=new BorderPane();
		layout.setBottom(messagePane);
		BorderPane messageInput=new BorderPane();
		messageInput.setCenter(message);
		messageInput.setPadding(new Insets(0, 5, 0, 0));
		messagePane.setCenter(messageInput);
		messagePane.setRight(submit);
		layout.setPadding(new Insets(5));
	}
	
	public String getName(){
		return name;
	}
	
	public void newMessage(String user,String message){
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				Text usr=new Text(user);
				usr.setFill(Color.DARKBLUE);
				Text msg=new Text(" : "+message+"\n");
				text.getChildren().add(usr);
				text.getChildren().add(msg);
			}
		});
	}
	
	private void submit(){
		MessageManager.sendMessage("PRIVMSG "+name+" :"+message.getText());
		MessageManager.parseMessage(ChatMain.getUsername()+"!cho@ppy.sh PRIVMSG "+name+" :"+message.getText());
		message.setText("");
	}
	
	
}
