package cf.nkg5.osuchat;


import java.util.HashMap;
import java.util.Map;

import javafx.scene.control.Tab;

public class MessageManager {
	private static Client client;
	private static Map<String, Chanel> chanels=new HashMap<String, Chanel>();
	
	public static void setClient(Client c){
		client=c;
	}
	
	public static void sendMessage(String message){
		if(client!=null)
			client.newMessage(message);		
	}
	
	public static void parseMessage(String message){
		if(message.startsWith("PING cho.ppy.sh")){
			sendMessage("PONG cho.ppy.sh");
		}
		if(message.contains("!cho@ppy.sh PRIVMSG")){
			String[] tmp=message.split("!cho@ppy.sh PRIVMSG ");
			tmp[0]=tmp[0].replaceFirst(":", "");
			tmp[0]=tmp[0].replace("_", " ");
			String[] msg=tmp[1].split(" :", 2);
			chanels.get(msg[0]).newMessage(tmp[0], msg[1]);
		}
	}
	
	public static Tab join(String name, boolean closable){
		Chanel chanel=new Chanel(name, closable);
		sendMessage("JOIN "+name);
		chanels.put(name, chanel);
		return chanel;
	}
}
