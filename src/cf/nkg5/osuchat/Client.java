package cf.nkg5.osuchat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;;

public class Client {

	private Socket socket=null;
	private PrintStream out=null;
	
	public Client(String username, String password){
		try {
			socket=new Socket("irc.ppy.sh",6667);
			out=new PrintStream(socket.getOutputStream());
			out.println("PASS "+password);
			out.println("NICK "+username);
			ChatMain.setUsername(username);
			out.flush();
		} catch (UnknownHostException e) {
			new Alert(AlertType.ERROR,"Unknown host.").showAndWait();
			System.exit(2);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(3);
		}
		
		Thread t=new Thread(new ClientRunnable());
		t.setDaemon(true);
		t.start();
		MessageManager.setClient(this);
	}
	
	public void newMessage(String message){
		if(out!=null&&message!=null){
			out.println(message);
			out.flush();
		}
	}
	

	private class ClientRunnable implements Runnable{
		@Override
		public void run() {
			try {
				BufferedReader in=new BufferedReader(new InputStreamReader(socket.getInputStream()));
				String line="";
				while((line=in.readLine())!=null){
					MessageManager.parseMessage(line);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
