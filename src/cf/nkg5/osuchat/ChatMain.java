package cf.nkg5.osuchat;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.Optional;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import javafx.util.Pair;

public class ChatMain extends Application{
	
	private static String username;
	
	public static String getUsername(){
		return username;
	}
	
	public static void setUsername(String username){
		ChatMain.username=username;
	}

	public static void main(String[] args) {
		
		
		try{
			PrintStream log=new PrintStream(new File("log.txt"));
			System.setErr(log);
		}catch(Exception e){
			System.exit(1);
		}
		
		
		launch(args);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage window) throws Exception {
		Pair<String, String> login=null;
		File data=new File("data.dat");
		if(data.exists()){
			try{
				BufferedReader in=new BufferedReader(new InputStreamReader(new FileInputStream("data.dat")));
				String line = in.readLine();
				byte[] bytes = Base64.getDecoder().decode(line);
				bytes = Base64.getDecoder().decode(bytes);
				ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
				ObjectInputStream ois = new ObjectInputStream(bis);
				login = (Pair<String, String>) ois.readObject();
				System.out.println(login);
				in.close();
			}catch(Exception e){
				System.out.println(e);
			}
		}
		if(login==null)
			login=showDialog();
		if(login!=null){
			new Client(login.getKey(), login.getValue());
			initWindow(window);
			window.show();
		}else
			stop();
	}

	private Pair<String,String> showDialog(){
		Dialog<Pair<String,String>> dialog=new Dialog<>();
		dialog.setTitle("Log in");
		dialog.setHeaderText("Please enter your osu! irc username and password.");
		
		Label uname=new Label("Username: ");
		Label pword=new Label("Password: ");
		
		TextField username=new TextField();
		PasswordField password=new PasswordField();
		
		uname.setLabelFor(username);
		pword.setLabelFor(password);
		
		GridPane layout=new GridPane();
		ColumnConstraints c0=new ColumnConstraints();
		c0.setHgrow(Priority.ALWAYS);
		layout.getColumnConstraints().add(c0);
		ColumnConstraints c1=new ColumnConstraints();
		c1.setHgrow(Priority.NEVER);
		layout.getColumnConstraints().add(c1);
		ColumnConstraints c2=new ColumnConstraints();
		c2.setHgrow(Priority.NEVER);
		layout.getColumnConstraints().add(c2);
		ColumnConstraints c3=new ColumnConstraints();
		c3.setHgrow(Priority.ALWAYS);
		layout.getColumnConstraints().add(c3);
		
		layout.setHgap(5);
		layout.setVgap(5);
		
		layout.add(uname, 1, 1);
		layout.add(pword, 1, 2);
		layout.add(username, 2, 1);
		layout.add(password, 2, 2);
		CheckBox remember=new CheckBox("Remember user");
		layout.add(remember, 2, 3);
		Text hint=new Text("You can get your password at ");
		Text link=new Text("http://osu.ppy.sh/p/irc");
		link.setFill(Color.BLUE);
		link.setUnderline(true);
		link.setCursor(Cursor.HAND);
		link.setOnMouseClicked(e->{
			getHostServices().showDocument("http://osu.ppy.sh/p/irc");
		});
		layout.add(new TextFlow(hint, link), 1, 4, 2, 1);
		dialog.getDialogPane().setContent(layout);
		
		ButtonType login=new ButtonType("Login",ButtonData.OK_DONE);
		
		dialog.getDialogPane().getButtonTypes().add(login);
		dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
		
		dialog.setResultConverter(button -> {
		    if (button == login){
		    	Pair<String, String> ret=new Pair<String, String>(username.getText(), password.getText());
		    	if(remember.isSelected()){
		    		try{
		    			ByteArrayOutputStream bos = new ByteArrayOutputStream();
		    			ObjectOutputStream out = new ObjectOutputStream(bos);
		    			out.writeObject(ret);
		    			out.close();
		    			byte[] bytes = bos.toByteArray();
		    			byte[] encBytes = Base64.getEncoder().encode(bytes);
		    			String output = Base64.getEncoder().encodeToString(encBytes);
		    			PrintWriter file = new PrintWriter(new FileOutputStream("data.dat"));
		    			file.println(output);
		    			file.close();
		    		}catch(Exception e){
		    			e.printStackTrace();
		    		}
		    	}
		        return ret;
		    }
		    return null;
		});
		Optional<Pair<String, String>> result=dialog.showAndWait();
		if(result.isPresent()){
			return result.get();
		}
		return null;
	}
	
	private void initWindow(Stage window){
		window.setTitle("Chat for osu!");
		TabPane tabs=new TabPane();
		tabs.getTabs().add(mainTab(tabs));
		tabs.getTabs().add(MessageManager.join("#osu",false));
		Scene scene=new Scene(tabs);
		window.setScene(scene);
		window.setMinHeight(480);
		window.setMinWidth(640);
	}
	
	private Tab mainTab(TabPane tabs){
		Tab tab=new Tab("Main");
		tab.setClosable(false);
		BorderPane layout=new BorderPane();
		layout.setPadding(new Insets(5));
		tab.setContent(layout);
		FlowPane chanels=new FlowPane(Orientation.VERTICAL);
		layout.setLeft(chanels);
		chanels.getChildren().add(new ChanelButton("#announce", tabs));
		chanels.getChildren().add(new ChanelButton("#arabic", tabs));
		chanels.getChildren().add(new ChanelButton("#balkan", tabs));
		chanels.getChildren().add(new ChanelButton("#bulgarian", tabs));
		chanels.getChildren().add(new ChanelButton("#cantonese", tabs));
		chanels.getChildren().add(new ChanelButton("#chinese", tabs));
		chanels.getChildren().add(new ChanelButton("#ctb", tabs));
		chanels.getChildren().add(new ChanelButton("#czechoslovak", tabs));
		chanels.getChildren().add(new ChanelButton("#dutch", tabs));
		chanels.getChildren().add(new ChanelButton("#english", tabs));
		chanels.getChildren().add(new ChanelButton("#filipino", tabs));
		chanels.getChildren().add(new ChanelButton("#finnish", tabs));
		chanels.getChildren().add(new ChanelButton("#french", tabs));
		chanels.getChildren().add(new ChanelButton("#german", tabs));
		chanels.getChildren().add(new ChanelButton("#greek", tabs));
		chanels.getChildren().add(new ChanelButton("#hebrew", tabs));
		chanels.getChildren().add(new ChanelButton("#help", tabs));
		chanels.getChildren().add(new ChanelButton("#hungarian", tabs));
		chanels.getChildren().add(new ChanelButton("#indonesian", tabs));
		chanels.getChildren().add(new ChanelButton("#italian", tabs));
		chanels.getChildren().add(new ChanelButton("#japanese", tabs));
		chanels.getChildren().add(new ChanelButton("#korean", tabs));
		chanels.getChildren().add(new ChanelButton("#lobby", tabs));
		chanels.getChildren().add(new ChanelButton("#malaysian", tabs));
		chanels.getChildren().add(new ChanelButton("#modhelp", tabs));
		chanels.getChildren().add(new ChanelButton("#modreqs", tabs));
		chanels.getChildren().add(new ChanelButton("#osumania", tabs));
		chanels.getChildren().add(new ChanelButton("#polish", tabs));
		chanels.getChildren().add(new ChanelButton("#portuguese", tabs));
		chanels.getChildren().add(new ChanelButton("#romanian", tabs));
		chanels.getChildren().add(new ChanelButton("#russian", tabs));
		chanels.getChildren().add(new ChanelButton("#skandinavian", tabs));
		chanels.getChildren().add(new ChanelButton("#spanish", tabs));
		chanels.getChildren().add(new ChanelButton("#taiko", tabs));
		chanels.getChildren().add(new ChanelButton("#thai", tabs));
		chanels.getChildren().add(new ChanelButton("#turkish", tabs));
		chanels.getChildren().add(new ChanelButton("#videogames", tabs));
		chanels.getChildren().add(new ChanelButton("#vietnamese", tabs));
		
		File data=new File("data.dat");
		if(data.exists()){
			BorderPane userPane=new BorderPane();
			layout.setBottom(userPane);
			userPane.setPadding(new Insets(5, 0, 0, 0));
			Button forget=new Button("Forget username and password");
			forget.setOnAction(e->{
				data.delete();
			});
			userPane.setRight(forget);
		}
		return tab;
	}
}


