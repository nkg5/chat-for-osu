package cf.nkg5.osuchat;

import javafx.scene.control.Button;
import javafx.scene.control.TabPane;

public class ChanelButton extends Button{
	private String name;
	private TabPane tabs;
	
	public ChanelButton(String name,TabPane tabs) {
		super(name);
		this.name=name;
		this.tabs=tabs;
		this.setOnAction(e->join());
		this.setPrefWidth(200);
	}
	
	private void join(){
		tabs.getTabs().add(MessageManager.join(name, true));
	}
}
